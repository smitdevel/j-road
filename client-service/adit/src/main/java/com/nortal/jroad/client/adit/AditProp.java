package com.nortal.jroad.client.adit;

/**
 * Created by Vassili on 5/22/2017.
 */
public interface AditProp {

	enum User {
		USER_CODE("code"),
		HAS_JOINED("has_joined"),
		USES_DVK("uses_dvk"),
		CAN_READ("can_read"),
		CAN_WRITE("can_write");

		private final String name;

		private User(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}

	enum Document {
		ID_CODE("isikukood"),
		REVEICER_NAME("saajaNimi"),
		OPEN_TIME("avamiseAeg"),
		OPENED("avatud");

		private final String name;

		private Document(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
	}

}
