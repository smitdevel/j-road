package com.nortal.jroad.client.adit;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.nortal.jroad.client.exception.XRoadServiceConsumptionException;

/**
 * Created by Vassili on 5/22/2017.
 * Updated by Aare.Puusepp on 02/12/2019
 */
public interface AditXTeeService {

  void confirmSignatureV1(String documentId) throws XRoadServiceConsumptionException;

  void deflateDocumentV1() throws XRoadServiceConsumptionException;

  void deleteDocumentFileV1() throws XRoadServiceConsumptionException;

  void deleteDocumentV1() throws XRoadServiceConsumptionException;

  void deleteDocumentsV1() throws XRoadServiceConsumptionException;

  void getDocumentFileV1() throws XRoadServiceConsumptionException;

  void getDocumentHistoryV1() throws XRoadServiceConsumptionException;

  void getDocumentListV1() throws XRoadServiceConsumptionException;

  void getDocumentV1() throws XRoadServiceConsumptionException;

  void getJoinedV1() throws XRoadServiceConsumptionException;

  void getNotificationsV1() throws XRoadServiceConsumptionException;

  /**
   * @param documents
   * @param currentUserId - id code to use in x-tee request. If id is null or blank then {@code value} from properties file will be used.
   * @return Map where keys are document ID's and values are lists of all recipients properties
   * @throws XRoadServiceConsumptionException
   */
  Map<String, List<Map<String, Serializable>>> getSendStatusV1(Set<String> documents, String currentUserId) throws XRoadServiceConsumptionException;

  void getUserContactsV1() throws XRoadServiceConsumptionException;

  /**
   * @param userIds
   * @param currentUserId - id code to use in x-tee request
   * @return Map where keys are userIds and values are user properties as a map
   * @throws XRoadServiceConsumptionException
   */
  Map<String, Map<String, Serializable>> getUserInfoV1(Set<String> userIds, String currentUserId) throws XRoadServiceConsumptionException;

  void joinV1() throws XRoadServiceConsumptionException;

  void markDocumentViewedV1() throws XRoadServiceConsumptionException;

  void modifyStatusV1() throws XRoadServiceConsumptionException;

  void prepareSignatureV1() throws XRoadServiceConsumptionException;

  void saveDocumentFileV1() throws XRoadServiceConsumptionException;

  void saveDocumentV1() throws XRoadServiceConsumptionException;

  void sendDocumentV1() throws XRoadServiceConsumptionException;

  void setNotificationsV1() throws XRoadServiceConsumptionException;

  void shareDocumentV1() throws XRoadServiceConsumptionException;

  void unJoinV1() throws XRoadServiceConsumptionException;

  void unShareDocumentV1() throws XRoadServiceConsumptionException;

}
