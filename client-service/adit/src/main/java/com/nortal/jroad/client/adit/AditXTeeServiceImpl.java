package com.nortal.jroad.client.adit;

import com.nortal.jroad.client.adit.types.ee.riik.xtee.ametlikud_dokumendid.producers.producer.ametlikud_dokumendid.*;
import com.nortal.jroad.client.adit.types.noNamespace.GetSendStatusRequestAttachmentV1;
import com.nortal.jroad.client.adit.types.noNamespace.GetUserInfoRequestAttachmentV1Type;
import com.nortal.jroad.client.exception.NonTechnicalFaultException;
import com.nortal.jroad.client.exception.XRoadServiceConsumptionException;
import com.nortal.jroad.client.service.XRoadDatabaseService;
import com.nortal.jroad.client.service.callback.CustomCallback;
import com.nortal.jroad.model.XRoadAttachment;
import com.nortal.jroad.model.XRoadMessage;
import com.nortal.jroad.model.XmlBeansXRoadMessage;
import com.nortal.jroad.util.AttachmentUtil;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.ws.commons.util.Base64;
import org.apache.xmlbeans.XmlCursor;
import org.apache.xmlbeans.XmlObject;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.soap.saaj.SaajSoapMessage;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.*;
import javax.xml.transform.TransformerException;
import java.io.*;
import java.lang.reflect.Constructor;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

@Service("ametlikudDokumendidXTeeService")
public class AditXTeeServiceImpl extends XRoadDatabaseService implements AditXTeeService {

    private static final org.apache.commons.logging.Log LOG = org.apache.commons.logging.LogFactory
        .getLog(AditXTeeServiceImpl.class);

    private static final String GET_SEND_STATUS = "getSendStatus";
    private static final String GET_USER_INFO = "getUserInfo";
    private static final String V_1 = "v1";
    private static final String XML_TYPE = "text/xml";

    private static final Map<String, Class<?>> PROP_CLASS_MAPPING = new HashMap<String, Class<?>>();

    static {
        PROP_CLASS_MAPPING.put(AditProp.User.HAS_JOINED.getName(), Boolean.class);
        PROP_CLASS_MAPPING.put(AditProp.User.USES_DVK.getName(), Boolean.class);
        PROP_CLASS_MAPPING.put(AditProp.User.CAN_READ.getName(), Boolean.class);
        PROP_CLASS_MAPPING.put(AditProp.User.CAN_WRITE.getName(), Boolean.class);
        PROP_CLASS_MAPPING.put(AditProp.Document.OPENED.getName(), Boolean.class);
        PROP_CLASS_MAPPING.put(AditProp.Document.OPEN_TIME.getName(), Date.class);
    }

    public static void main(String args[]) {

        SOAPElement element = null;
        try {
            element = SOAPElementFactory.newInstance().create("infosysteem", "adit", "http://producers.ametlikud-dokumendid.xtee.riik.ee/producer/ametlikud-dokumendid");
        } catch (SOAPException e) {
            e.printStackTrace();
        }
        element.setValue("Delta");
        LOG.debug("1=" + element.getValue());
    }

    public static byte[] gzipAndEncodeString(String inputString) {
        OutputStream gzipBase64OutputStream = null;
        ByteArrayOutputStream gzippedAndEncodedOutputStream = new ByteArrayOutputStream();
        Writer writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(gzippedAndEncodedOutputStream, "UTF-8"));
            OutputStream base64OutputStream = Base64.newEncoder(writer, 0, null);
            gzipBase64OutputStream = new GZIPOutputStream(base64OutputStream);
            IOUtils.write(inputString, gzipBase64OutputStream, "UTF-8");
        } catch (IOException e1) {
            throw new RuntimeException("Failed to encode input", e1);
        } finally {
            IOUtils.closeQuietly(gzipBase64OutputStream);
            IOUtils.closeQuietly(writer);
        }
        return gzippedAndEncodedOutputStream.toByteArray();
    }

    @Override
    public Map<String, Map<String, Serializable>> getUserInfoV1(Set<String> userIdCodes, String infoSystem)
        throws XRoadServiceConsumptionException {
        if (CollectionUtils.isEmpty(userIdCodes)) {
            LOG.warn("No user id codes were provided, query was not executed.");
            return Collections.emptyMap();
        }
        String cid = AttachmentUtil.getUniqueCid();
        GetUserInfo request = GetUserInfo.Factory.newInstance();
        request.addNewKeha().addNewUserList().setHref(cid);
        XRoadMessage<GetUserInfoResponse> response = null;

        try {

            XRoadAttachment requestAttachment = createGZippedAndEncodedUserListAttachment(userIdCodes, cid,
                "getUserInfoRequestAttachmentV1");

            response = send(new XmlBeansXRoadMessage<GetUserInfo>(request, Arrays.asList(requestAttachment)), GET_USER_INFO,
                V_1, new AditCallback(infoSystem), null);
        } catch (XRoadServiceConsumptionException e) {
            LOG.error("Unable to make request: ", e);
            throw e;
        }

        if (response == null || !response.getContent().getKeha().getSuccess()) {
            extractFaultMessageAndThrow(response, GET_USER_INFO, V_1);
        }

        List<XRoadAttachment> responseAttachments = response.getAttachments();
        if (!CollectionUtils.isEmpty(responseAttachments)) {
            try {
                String xml = unGZip(responseAttachments);
                return parseUserInfoResponse(xml);
            } catch (Exception e) {
                LOG.warn(e);
                throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("1", e.getMessage()), getDatabase(), GET_USER_INFO,
                    V_1);
            }
        }
        return Collections.emptyMap();
    }

    @Override
    public void confirmSignatureV1(String documentId) throws XRoadServiceConsumptionException {
        throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("1", "Not implemented jet...!"), getDatabase(), "confirmSignature", V_1);
    }

    @Override
    public void deflateDocumentV1() throws XRoadServiceConsumptionException {
        throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("1", "Not implemented jet...!"), getDatabase(), "deflateDocument", V_1);
    }

    @Override
    public void deleteDocumentFileV1() throws XRoadServiceConsumptionException {
        throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("1", "Not implemented jet...!"), getDatabase(), "deleteDocumentFile", V_1);
    }

    @Override
    public void deleteDocumentV1() throws XRoadServiceConsumptionException {
        throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("1", "Not implemented jet...!"), getDatabase(), "deleteDocument", V_1);
    }

    @Override
    public void deleteDocumentsV1() throws XRoadServiceConsumptionException {
        throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("1", "Not implemented jet...!"), getDatabase(), "deleteDocuments", V_1);
    }

    @Override
    public void getDocumentFileV1() throws XRoadServiceConsumptionException {
        throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("1", "Not implemented jet...!"), getDatabase(), "getDocumentFile", V_1);
    }

    @Override
    public void getDocumentHistoryV1() throws XRoadServiceConsumptionException {
        throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("1", "Not implemented jet...!"), getDatabase(), "getDocumentHistory", V_1);
    }

    @Override
    public void getDocumentListV1() throws XRoadServiceConsumptionException {
        throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("1", "Not implemented jet...!"), getDatabase(), "getDocumentList", V_1);
    }

    @Override
    public void getDocumentV1() throws XRoadServiceConsumptionException {
        throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("1", "Not implemented jet...!"), getDatabase(), "getDocument", V_1);
    }

    @Override
    public void getJoinedV1() throws XRoadServiceConsumptionException {
        throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("1", "Not implemented jet...!"), getDatabase(), "getJoined", V_1);
    }

    @Override
    public void getNotificationsV1() throws XRoadServiceConsumptionException {
        throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("1", "Not implemented jet...!"), getDatabase(), "getNotifications", V_1);
    }

    @Override
    public Map<String, List<Map<String, Serializable>>> getSendStatusV1(Set<String> documents, String infoSystem)
        throws XRoadServiceConsumptionException {
        if (CollectionUtils.isEmpty(documents)) {
            LOG.warn("Set of documents was null or empty!");
            return Collections.emptyMap();
        }
        String cid = AttachmentUtil.getUniqueCid();
        GetSendStatus request = GetSendStatus.Factory.newInstance();
        request.addNewKeha().addNewDocumendid().setHref(cid);
        XRoadMessage<GetSendStatusResponse> response = null;

        try {
            XRoadAttachment requestAttachment = createGZippedAndEncodedSendStatusAttachment(documents, cid);
            response = send(new XmlBeansXRoadMessage<GetSendStatus>(request, Arrays.asList(requestAttachment)), GET_SEND_STATUS,
                V_1, new AditCallback(infoSystem), null);
        } catch (XRoadServiceConsumptionException e) {
            LOG.warn("Unable to make request: ", e);
            throw e;
        }

        if (response == null || !response.getContent().getKeha().getSuccess()) {
            extractFaultMessageAndThrow(response, GET_SEND_STATUS, V_1);
        }

        List<XRoadAttachment> responseAttachments = response.getAttachments();
        if (!CollectionUtils.isEmpty(responseAttachments)) {
            try {
                String xml = unGZip(responseAttachments);
                return parseSendStatusResponse(xml, documents);
            } catch (Exception e) {
                LOG.warn(e);
                throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("2", e.getMessage()), getDatabase(),
                    GET_SEND_STATUS, V_1);
            }
        }
        return Collections.emptyMap();
    }

    @Override
    public void joinV1() throws XRoadServiceConsumptionException {
        throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("1", "Not implemented jet...!"), getDatabase(), "join", V_1);
    }

    @Override
    public void markDocumentViewedV1() throws XRoadServiceConsumptionException {
        throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("1", "Not implemented jet...!"), getDatabase(), "markDocumentViewed", V_1);
    }

    @Override
    public void modifyStatusV1() throws XRoadServiceConsumptionException {
        throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("1", "Not implemented jet...!"), getDatabase(), "modifyStatus", V_1);
    }

    @Override
    public void prepareSignatureV1() throws XRoadServiceConsumptionException {
        throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("1", "Not implemented jet...!"), getDatabase(), "prepareSignature", V_1);
    }

    @Override
    public void saveDocumentFileV1() throws XRoadServiceConsumptionException {
        throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("1", "Not implemented jet...!"), getDatabase(), "saveDocumentFile", V_1);
    }

    @Override
    public void saveDocumentV1() throws XRoadServiceConsumptionException {
        throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("1", "Not implemented jet...!"), getDatabase(), "saveDocument", V_1);
    }

    @Override
    public void sendDocumentV1() throws XRoadServiceConsumptionException {
        throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("1", "Not implemented jet...!"), getDatabase(), "sendDocument", V_1);
    }

    @Override
    public void setNotificationsV1() throws XRoadServiceConsumptionException {
        throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("1", "Not implemented jet...!"), getDatabase(), "setNotifications", V_1);
    }

    @Override
    public void shareDocumentV1() throws XRoadServiceConsumptionException {
        throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("1", "Not implemented jet...!"), getDatabase(), "shareDocument", V_1);
    }

    @Override
    public void unJoinV1() throws XRoadServiceConsumptionException {
        throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("1", "Not implemented jet...!"), getDatabase(), "unJoin", V_1);
    }

    @Override
    public void unShareDocumentV1() throws XRoadServiceConsumptionException {
        throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("1", "Not implemented jet...!"), getDatabase(), "unShareDocument", V_1);
    }

    @Override
    public void getUserContactsV1() throws XRoadServiceConsumptionException {
        throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("1", "Not implemented jet...!"), getDatabase(), "getUserContacts", V_1);
    }

    private void extractFaultMessageAndThrow(XRoadMessage<? extends XmlObject> response, String method, String version)
        throws XRoadServiceConsumptionException {
        if (response == null) {
            throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("3", "Unknown exception"), getDatabase(), method,
                version);
        }
        List<Message> messageList;
        XmlObject content = response.getContent();
        if (content instanceof GetSendStatusResponse) {
            messageList = ((GetSendStatusResponse) content).getKeha().getMessages().getMessageList();
        } else {
            messageList = ((GetUserInfoResponse) content).getKeha().getMessages().getMessageList();
        }
        String defaultLang = "en";
        String preferedLang = "et";
        String errorMessage = getErrorMessage(messageList, preferedLang);
        if (StringUtils.isBlank(errorMessage)) {
            errorMessage = getErrorMessage(messageList, defaultLang);
        }
        throw new XRoadServiceConsumptionException(new NonTechnicalFaultException("4", errorMessage), null, method, version);
    }

    private String getErrorMessage(List<Message> messageList, String language) {
        String errorMessage = "";
        for (Message mess : messageList) {
            if (StringUtils.equalsIgnoreCase(language, mess.getLang())) {
                return mess.getStringValue();
            }
        }
        return errorMessage;
    }

    private XRoadAttachment createGZippedAndEncodedUserListAttachment(Set<String> userIds, String cid, String... wrapperElements) {
        String xmlHeader = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
        GetUserInfoRequestAttachmentV1Type attachment = GetUserInfoRequestAttachmentV1Type.Factory.newInstance();
        attachment.addNewUserList().setCodeArray(userIds.toArray(new String[userIds.size()]));
        StringBuilder attachmentBuilder = new StringBuilder();
        attachmentBuilder.append(xmlHeader);
        for (String element : wrapperElements) {
            attachmentBuilder.append("<" + element + ">");
        }
        attachmentBuilder.append(attachment.toString());
        for (String element : wrapperElements) {
            attachmentBuilder.append("</" + element + ">");
        }
        return new XRoadAttachment(cid, XML_TYPE, gzipAndEncodeString(attachmentBuilder.toString()));
    }

    private XRoadAttachment createGZippedAndEncodedSendStatusAttachment(Set<String> documentIds, String cid) {
        GetSendStatusRequestAttachmentV1 attachment = GetSendStatusRequestAttachmentV1.Factory.newInstance();
        XmlCursor cursor = attachment.newCursor();
        cursor.toNextToken();
        cursor.beginElement("item");
        for (String value : documentIds.toArray(new String[0])) {
            cursor.insertElementWithText("dhl_id", value);
        }
        cursor.dispose();
        return new XRoadAttachment(cid, XML_TYPE, gzipAndEncodeString(attachment.toString()));
    }

    private String unGZip(List<XRoadAttachment> responseAttachments) throws IOException {
        byte[] uncoded = responseAttachments.get(0).getData();
        //byte[] uncoded = Base64.decode(new String(encoded));
        Writer writer = new StringWriter();
        GZIPInputStream unzip = null;
        try {
            unzip = new GZIPInputStream(new ByteArrayInputStream(uncoded));
            IOUtils.copy(unzip, writer);
        } finally {
            IOUtils.closeQuietly(unzip);
            IOUtils.closeQuietly(writer);
        }
        return writer.toString();
    }

    private Map<String, Map<String, Serializable>> parseUserInfoResponse(String response)
        throws IOException, ParserConfigurationException, SAXException {
        Document doc = getDocument(response);
        Map<String, Map<String, Serializable>> result = new HashMap<String, Map<String, Serializable>>();

        NodeList userList = doc.getElementsByTagName("user");
        int nodes = userList.getLength();

        for (int i = 0; i < nodes; i++) {
            Map<String, Serializable> userProps = new HashMap<String, Serializable>();
            Element user = (Element) userList.item(i);
            extractProperties(userProps, user);
            String code = extractElement(user, AditProp.User.USER_CODE.getName());
            result.put(code, userProps);
        }
        return result;
    }

    private Map<String, List<Map<String, Serializable>>> parseSendStatusResponse(String response, Set<String> documentIds)
        throws IOException, ParserConfigurationException, SAXException {
        Document doc = getDocument(response);
        Map<String, List<Map<String, Serializable>>> result = new HashMap<String, List<Map<String, Serializable>>>();

        NodeList documentList = doc.getElementsByTagName("dokument");
        int nodes = documentList.getLength();

        for (int i = 0; i < nodes; i++) {
            Element dokElement = (Element) documentList.item(i);
            if (dokElement == null) {
                continue;
            }
            String dhlId = extractElement(dokElement, "dhl_id");

            NodeList receiverList = dokElement.getElementsByTagName("saaja");
            int receivers = receiverList.getLength();
            List<Map<String, Serializable>> receiverArrayList = new ArrayList<Map<String, Serializable>>();

            for (int j = 0; j < receivers; j++) {
                Element receiver = (Element) receiverList.item(j);
                if (receiver == null) {
                    continue;
                }
                Map<String, Serializable> receiverProps = new HashMap<String, Serializable>();
                extractProperties(receiverProps, receiver);
                receiverArrayList.add(receiverProps);
            }
            result.put(dhlId, receiverArrayList);
        }

        // add all non-valid dhl_id's to result
        Set<String> results = result.keySet();
        documentIds.removeAll(results);
        for (String docId : documentIds) {
            result.put(docId, Collections.<Map<String, Serializable>>emptyList());
        }
        return result;
    }

    private void extractProperties(Map<String, Serializable> userProps, Element node) {
        NodeList props = node.getChildNodes();
        for (int j = 0; j < props.getLength(); j++) {
            if (!(props.item(j) instanceof Element)) {
                continue;
            }
            Element prop = (Element) props.item(j);
            String propName = prop.getTagName();
            if ("messages".equals(propName)) {
                continue;
            }
            String value = prop.getTextContent();
            if (PROP_CLASS_MAPPING.containsKey(propName)) {
                try {
                    Class<?> clazz = PROP_CLASS_MAPPING.get(propName);
                    if (Date.class.equals(clazz)) {
                        Date formatedDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(value);
                        userProps.put(prop.getTagName(), formatedDate);
                    } else {
                        Constructor<?> constructor = clazz.getConstructor(String.class);
                        userProps.put(prop.getTagName(), (Serializable) constructor.newInstance(new Object[]{value}));
                    }
                } catch (Exception e) {
                    userProps.put(prop.getTagName(), value);
                }
            } else {
                userProps.put(prop.getTagName(), value);
            }
        }
    }

    private Document getDocument(String response) throws ParserConfigurationException, SAXException, IOException {
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = builder.parse(new InputSource(new StringReader(response)));
        return doc;
    }

    private String extractElement(Element elements, String elementName) {
        Element element = (Element) elements.getElementsByTagName(elementName).item(0);
        return element == null ? null : element.getTextContent();
    }

    private class AditCallback extends CustomCallback {

        private String infoSystem;

        public AditCallback(String infoSystem) {
            this.infoSystem = infoSystem;
        }

        @Override
        public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {
            this.callback.doWithMessage(message);
            SaajSoapMessage saajMessage = (SaajSoapMessage) message;

            try {

                SOAPMessage soapmess = saajMessage.getSaajMessage();
                SOAPEnvelope env = soapmess.getSOAPPart().getEnvelope();
                SOAPHeader header = env.getHeader();
                SOAPElement element = header.addChildElement("infosysteem", "ns5");
                element.setValue(infoSystem);
            } catch (SOAPException var5) {
                throw new RuntimeException(var5);
            }


        }
    }
}
