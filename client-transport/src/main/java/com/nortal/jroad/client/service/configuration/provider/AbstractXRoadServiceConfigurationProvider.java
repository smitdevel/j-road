package com.nortal.jroad.client.service.configuration.provider;

import com.nortal.jroad.client.service.configuration.SimpleXRoadServiceConfiguration;
import com.nortal.jroad.client.service.configuration.XRoadServiceConfiguration;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * @author Aleksei Bogdanov (aleksei.bogdanov@nortal.com)
 * @author Lauri Lättemäe (lauri.lattemae@nortal.com) - protocol 4.0
 */
public abstract class AbstractXRoadServiceConfigurationProvider implements XRoadServiceConfigurationProvider {
  private static Logger logger = LogManager.getLogger(AbstractXRoadServiceConfigurationProvider.class);

  public static final String XROAD_PROTOCOL_VERSION_FORMAT = "%s-protocol-version";
  public static final String XROAD_INSTANCE_FORMAT = "%s-xroad-instance";
  public static final String XROAD_MEMBER_CLASS_FORMAT = "%s-member-class";
  public static final String XROAD_SUBSYSTEM_CODE_FORMAT = "%s-subsystem-code";
  public static final String XROAD_MEMBER_CODE_FORMAT = "%s-member-code";
  public static final String XROAD_OBJECT_TYPE_FORMAT = "%s-object-type";

  @Override
  public XRoadServiceConfiguration createConfiguration(String database,
                                                       String wsdlDatabase,
                                                       String method,
                                                       String version) {
    SimpleXRoadServiceConfiguration configuration = new SimpleXRoadServiceConfiguration();
    logger.debug("Set database: " + database);
    configuration.setDatabase(database);
    logger.debug("Set WSDL database: " + wsdlDatabase);
    configuration.setWsdlDatabase(wsdlDatabase);
    logger.debug("Set method: " + method);
    configuration.setMethod(method);
    logger.debug("Set version: " + version);
    configuration.setVersion(version);

    return fillConfuguration(configuration);
  }

  protected String getKey(String pattern, String value) {
    return String.format(pattern, value);
  }

  protected abstract XRoadServiceConfiguration fillConfuguration(SimpleXRoadServiceConfiguration configuration);
}
