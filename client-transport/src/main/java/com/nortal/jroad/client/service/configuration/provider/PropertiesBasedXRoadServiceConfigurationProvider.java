package com.nortal.jroad.client.service.configuration.provider;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.nortal.jroad.client.enums.XroadObjectType;
import com.nortal.jroad.client.service.configuration.SimpleXRoadServiceConfiguration;
import com.nortal.jroad.client.service.configuration.XRoadServiceConfiguration;
import com.nortal.jroad.client.util.PropertiesUtil;
import com.nortal.jroad.enums.XRoadProtocolVersion;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.UrlResource;

/**
 * @author Aleksei Bogdanov (aleksei.bogdanov@nortal.com)
 * @author Lauri Lättemäe (lauri.lattemae@nortal.com) - protocol 4.0
 */
public class PropertiesBasedXRoadServiceConfigurationProvider extends AbstractXRoadServiceConfigurationProvider {
    private static Logger logger = LogManager.getLogger(PropertiesBasedXRoadServiceConfigurationProvider.class);

    public static final String XROAD_DATABASE_PROPERTIES_FORMAT = "xroad-%s.properties";
    public static final String XROAD_CLIENT_PROPERTIES = "xroad.properties";
    public static final String CLIENT_KEY = "client";

    private Resource resource;
    private String resourcePath;
    private boolean externalXroadProp = true;

    private Map<String, Properties> properties = new HashMap<String, Properties>();

    @PostConstruct
    public void init() {
        logger.debug("TRY TO READ " + resourcePath + XROAD_CLIENT_PROPERTIES + " FROM FILE...");
        if(!readFromFile()){
            externalXroadProp = false;
            if (resource == null) {
                logger.debug("TRY TO READ " + XROAD_CLIENT_PROPERTIES + " FROM RESOURCE...");
                resource = new ClassPathResource(XROAD_CLIENT_PROPERTIES);
            }

            if (!readXroadProperties(resource)) {
                throw new IllegalStateException("Unable to resolve configuration properties: " + resource.getFilename());
            }

        }

    }

    private boolean readFromFile(){
        try {
            logger.debug("-- readFromFile --------------");
            File file = new File(resourcePath + XROAD_CLIENT_PROPERTIES);

            return readXroadProperties(file);
        } catch (Exception e){
            logger.debug(e.getMessage());
            logger.debug(e.getStackTrace().toString());
            return false;
        }
    }

    private boolean readXroadProperties(Object object){
        try{
            if(object instanceof  Resource){
                logger.debug(" -- XROAD.PROPERTIES file type: RESOURCE");
                properties.put(XROAD_CLIENT_PROPERTIES, loadProperties((Resource) object));
            } else if(object instanceof File){
                logger.debug(" -- XROAD.PROPERTIES file type: FILE ");
                properties.put(XROAD_CLIENT_PROPERTIES, loadProperties((File) object));
            }

        } catch (Exception e){
            return false;
        }
        return true;
    }

    @Override
    protected XRoadServiceConfiguration fillConfuguration(SimpleXRoadServiceConfiguration configuration) {
        logger.debug("GET security-server: " + getProperty("security-server"));
        configuration.setSecurityServer(getProperty("security-server"));
        logger.debug("GET id-code: " + getProperty("id-code"));
        configuration.setIdCode(getProperty("id-code"));
        logger.debug("GET file: " + getProperty("file"));
        configuration.setFile(getProperty("file"));

        logger.debug("Fill client properties...");
        fillClientProperties(configuration);
        logger.debug("Fill service properties...");
        fillServiceProperties(configuration);

        return configuration;
    }

    protected void fillClientProperties(SimpleXRoadServiceConfiguration configuration) {
        logger.debug("GET client xroad instance: " + getClientProperty(XROAD_INSTANCE_FORMAT));
        configuration.setClientXRoadInstance(getClientProperty(XROAD_INSTANCE_FORMAT));
        logger.debug("GET client member class: " + getClientProperty(XROAD_MEMBER_CLASS_FORMAT));
        configuration.setClientMemberClass(getClientProperty(XROAD_MEMBER_CLASS_FORMAT));
        logger.debug("GET client client member code: " + getClientProperty(XROAD_MEMBER_CODE_FORMAT));
        configuration.setClientMemberCode(getClientProperty(XROAD_MEMBER_CODE_FORMAT));
        logger.debug("GET client subsystem code: " + getClientProperty(XROAD_SUBSYSTEM_CODE_FORMAT));
        configuration.setClientSubsystemCode(getClientProperty(XROAD_SUBSYSTEM_CODE_FORMAT));

        String objectType = getClientProperty(XROAD_OBJECT_TYPE_FORMAT);
        logger.debug("GET objectType: " + objectType);
        if (StringUtils.isNotBlank(objectType)) {
            configuration.setClientObjectType(XroadObjectType.valueOf(objectType));
        }
    }

    protected void fillServiceProperties(SimpleXRoadServiceConfiguration configuration) {
        String db = configuration.getDatabase();
        configuration.setProtocolVersion(XRoadProtocolVersion.getValueByVersionCode(getServiceProperty(XROAD_PROTOCOL_VERSION_FORMAT,
            db)));
        logger.debug("GET service protocol version: " + configuration.getProtocolVersion());
        configuration.setServiceXRoadInstance(getServiceProperty(XROAD_INSTANCE_FORMAT, db));

        logger.debug("GET service member class: " + configuration.getServiceMemberClass());
        configuration.setServiceMemberClass(getServiceProperty(XROAD_MEMBER_CLASS_FORMAT, db));

        logger.debug("GET service member code: " + configuration.getServiceMemberCode());
        configuration.setServiceMemberCode(getServiceProperty(XROAD_MEMBER_CODE_FORMAT, db));

        logger.debug("GET service subsystem code: " + configuration.getServiceSubsystemCode());
        configuration.setServiceSubsystemCode(getServiceProperty(XROAD_SUBSYSTEM_CODE_FORMAT, db));
        String objectType = getServiceProperty(XROAD_OBJECT_TYPE_FORMAT, db);
        logger.debug("GET service object type: " + objectType);
        if (StringUtils.isNotBlank(objectType)) {
            configuration.setServiceObjectType(XroadObjectType.valueOf(objectType));
        }
    }

    protected String getClientProperty(String pattern) {
        logger.debug("GET CLIENT PROPERTY: PATTERN: " + pattern);
        return getProperty(XROAD_CLIENT_PROPERTIES, getKey(pattern, CLIENT_KEY));
    }

    protected String getServiceProperty(String pattern, String db) {
        logger.debug("GET SERVICE PROPERTY: PATTERN: " + pattern + "; DB: " + db);
        return getProperty(getKey(XROAD_DATABASE_PROPERTIES_FORMAT, db), getKey(pattern, db));
    }

    protected String getProperty(String key) {
        logger.debug("GET PROPERTY: KEY: " + key);
        return getProperty(XROAD_CLIENT_PROPERTIES, key);
    }

    protected String getProperty(String target, String key) {
        logger.debug("GET PROPERTY: Target: " + target + "; KEY: " + key);
        String result = properties.get(XROAD_CLIENT_PROPERTIES).getProperty(key);
        if (StringUtils.isNotBlank(result)) {
            logger.debug("RETURN PROPERTIES VALUE: " + result);
            return result;
        }
        return loadProperties(target).getProperty(key);
    }

    protected synchronized Properties loadProperties(String target) {
        logger.debug("LOAD PROPERTIES....");
        logger.debug("LOAD PROPERTIES....");
        if (!properties.containsKey(target)) {
            logger.debug("TARGET (" + target + ") is not in properties list.. Add new...");
            if(externalXroadProp){
                File file = new File(resourcePath + target);
                properties.put(target, loadProperties(file));
            } else {
                properties.put(target, loadProperties(new ClassPathResource(target)));
            }

        }
        return properties.get(target);
    }

    protected Properties loadProperties(Resource resource) {
        logger.debug("-- LOAD PROPERTIES... from RESOURCE!");
        try {
            return PropertiesUtil.readProperties(resource);
        } catch (IOException e) {
            logger.debug("ERROR: " + e.getMessage());
            throw new IllegalStateException("Unable to resolve configuration properties: " + resource.getFilename());
        }
    }

    protected Properties loadProperties(File file){
        logger.debug("-- LOAD PROPERTIES... from FILE!");
        try{
            return PropertiesUtil.readProperties(file);
        } catch (IOException e) {
            throw new IllegalStateException("Unable to resolve configuration properties: " + resource.getFilename());
        }
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    public void setResourcePath(String resourcePath) {
        if(!resourcePath.endsWith("/")){
            resourcePath = resourcePath + "/";
        }
        this.resourcePath = resourcePath;
    }
}
