package com.nortal.jroad.client.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.util.DefaultPropertiesPersister;

/**
 * @author Aleksei Bogdanov (aleksei.bogdanov@nortal.com)
 */
public class PropertiesUtil {
    private static Logger logger = LogManager.getLogger(PropertiesUtil.class);
    
    public static Properties readProperties(Resource resource) throws IOException {
        logger.debug(" -- READ PROPERTIES... ");
        if (resource == null) {
            logger.debug(".. RESOURCE IS NULL!");
            return null;
        }
        logger.debug("-- RESOURCE DATA -- START! ------------------");
        //readFileData(resource.getFile());
        logger.debug("-- RESOURCE DATA -- FIN! --------------------");

        Properties props = new Properties();
        new DefaultPropertiesPersister().load(props, resource.getInputStream());
        return props;
    }

    public static Properties readProperties(File file) throws IOException {
        logger.debug(" -- READ PROPERTIES... ");
        if (file == null) {
            logger.debug("... FILE IS NULL!");
            return null;
        }

        logger.debug("-- FILE DATA -- START! ------------------");
        readFileData(file);
        logger.debug("-- FILE DATA -- FIN! --------------------");

        Properties props = new Properties();
        InputStream stream = new FileInputStream(file);
        new DefaultPropertiesPersister().load(props, stream);
        return props;
    }
    public static void readFileData(File file) {
        try{
            logger.debug(" -- FILE: Can READ: " + file.canRead());
            logger.debug(" -- FILE: Can WRITE: " + file.canWrite());
            logger.debug(" -- FILE: Can EXECUTE: " + file.canExecute());
            logger.debug(" -- FILE: Absolute PATH: " + file.getAbsolutePath());
            logger.debug(" -- FILE: Canonical PATH: " + file.getCanonicalPath());
            logger.debug(" -- FILE: Name: " + file.getName());
            logger.debug(" -- FILE: Parent: " + file.getParent());
            logger.debug(" -- FILE: Path: " + file.getPath());
        } catch (MalformedURLException e) {
            logger.error(e.getMessage());
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        try (FileInputStream fis = new FileInputStream(file)) {

            logger.debug("Total file size to read (in bytes) : " + fis.available());

            int content;
            while ((content = fis.read()) != -1) {
                System.out.print((char) content);
            }

        } catch (IOException e) {
            logger.warn(e.getMessage());
        }
    }

}
